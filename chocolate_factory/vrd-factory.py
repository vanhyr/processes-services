from threading import Thread, Semaphore
from time import sleep
from random import uniform


NUMBER_OF_WORKERS = 10
NUMBER_OF_TRUCKS = 40
WEEK_DAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

pallets = Semaphore(0) #Chocolate pallets ready to pick up


class Worker(Thread):
    '''Simulates the work of the factory employees producing the pallets
    of chocolate, as well as their rest periods'''

    WORKLEG_TIME = 8
    REST_TIME = 12
    PALLETS_PER_TIMEUNIT = 5

    def __init__(self, number):
        Thread.__init__(self)
        self.name = str(number)

    def goToFactory(self):
        print("[WORKER " + self.name + "] Going to work...")
        sleep(uniform(1, 3))

    def work(self):
        print("[WORKER " + self.name + "] Producing chocolate...")
        # Fill...
        for i in range(self.WORKLEG_TIME):
            sleep(1)
            pallets.release(self.PALLETS_PER_TIMEUNIT)
        print("TOTAL PALLETS: " + str(pallets._value))
        print("[WORKER " + self.name + "] Resting for 1 hour")
        sleep(1)

    def rest(self):
        print("[WORKER " + self.name + "] End of journey, heading home to rest")
        # Fill...
        sleep(self.REST_TIME)

    def run(self):
        # Fill...
        for i in range(len(WEEK_DAYS)):
            self.goToFactory()
            self.work()
            self.rest()
        

class Truck(Thread):
    '''Simulates the behaviour of the trucks that come to the factory
    to carry the chocolate pallets'''

    MAX_CAPACITY = 25

    def __init__(self, number):
        Thread.__init__(self)
        self.name = str(number)

    def travel(self):
        print("[TRUCK " + self.name + "] Going to the factory...")
        # Fill...
        sleep(uniform(1, 10))
        
    def load(self):
        print("[TRUCK " + self.name + "] Picking up pallets...")
        # Fill...
        for i in range(self.MAX_CAPACITY):
            pallets.acquire()
        print("TOTAL PALLETS: " + str(pallets._value))

    def run(self):
        # Fill...
        first_travel = uniform(1, 60)
        sleep(first_travel)
        print("[TRUCK " + self.name + "] First travel took: " + str(first_travel))
        self.travel()
        self.load()
        second_travel = 48
        sleep(second_travel)
        print("[TRUCK " + self.name + "] Second travel took: " + str(second_travel))
        self.travel()
        self.load()
        

if __name__ == "__main__":
    print("Chocolate factory")

    # Fill (create threads and launch them)
    for i in range(NUMBER_OF_WORKERS):
        worker = Worker(i)
        worker.start()

    for i in range(NUMBER_OF_TRUCKS):
        truck = Truck(i)
        truck.start()
