import time
#import timeit
from multiprocessing import Process
from sys import argv


class CountVowels(object):

    def __init__(self, file_name):
        self._file_name = file_name    

    def count_vowel(self, vowel):

        file = open(self._file_name, "rt", encoding='utf-8')
        
        count = 0
        
        while True:
            try:
                # read by char
                char = file.read(1)
                
                if char.lower() == vowel:
                    count += 1
                    
                if not char:
                    break
            except UnicodeDecodeError:
                continue
                
        file.close()

        return vowel, count


    def count_vowels(self):

        file = open(self._file_name, "r")
        
        vowels = {
            "a": 0,
            "e": 0,
            "i": 0,
            "o": 0,
            "u": 0,
        }

        while True:
            # read by char
            char = file.read(1)
            
            if char.lower() == "a":
                vowels["a"] += 1
            elif char.lower() == "e":
                vowels["e"] += 1
            elif char.lower() == "i":
                vowels["i"] += 1
            elif char.lower() == "o":
                vowels["o"] += 1
            elif char.lower() == "u":
                vowels["u"] += 1
                
            if not char:
                break
                
        file.close()

        return vowels


    def show_count(self, vowel, count):
        
        print(f"{vowel}: {str(count)}")


    def show_results(self, vowels):
        
        for vowel, count in vowels.items():
            print(f"{vowel}: {str(count)}") 


class SubProcess(Process):

    def __init__(self, cw, vowel):
        super().__init__()
        self._cw = cw
        self._vowel = vowel

    def run(self):
        vowel, count = self._cw.count_vowel(self._vowel)
        self._cw.show_count(vowel, count)


def _run(cw):

    # It may have 16ms error
    start_time = time.perf_counter()
    #start_time = timeit.default_timer()

    sp1 = SubProcess(cw, "a")
    sp2 = SubProcess(cw, "e")
    sp3 = SubProcess(cw, "i")
    sp4 = SubProcess(cw, "o")
    sp5 = SubProcess(cw, "u")

    print(f"----------------[Vowels count in file]----------------\n")
    
    sp1.start()
    sp2.start()
    sp3.start()
    sp4.start()
    sp5.start()

    # Waits for the subprocess to end execution
    sp1.join()
    sp2.join()
    sp3.join()
    sp4.join()
    sp5.join()

    # It may have 16ms error
    execution_time = time.perf_counter() - start_time
    #execution_time = timeit.default_timer() - start_time
    print(f"\nExecution time was: {str(round(execution_time, 4))} seconds.")


if __name__ == "__main__":

    args = argv[1:]

    if len(args) == 1:
        try:
            cw = CountVowels(args[0])
            _run(cw)
        except FileNotFoundError:
            print("Please enter a valid file or none for default")
    elif len(args) == 0:
        cw = CountVowels("lorem_ipsum.txt")
        _run(cw)
