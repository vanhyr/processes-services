Puedo observar dos comportamientos:

1) Archivos de texto no muy largos

  En este caso no se aprovecha el multiproceso porque tarda mucho en crear los subprocesos y la lectura con un único proceso
  es más rapida.

2) Archivos de texto largos

  En este caso se aprovecha el multiproceso ya que al ser más grande el archivo, le da tiempo a actuar a cada subproceso y
  el tiempo de ejecución en multiproceso es menor que en monoproceso.