from threading import Thread, Lock
from time import sleep
from random import uniform, choice

NUMBER_OF_CUSTOMERS = 2000


class Till:
    '''Simulates the use of a shop checkout for the available offers'''

    available_products = 1000

    @classmethod
    def hasProducts(cls):
        '''Tells if there are products on sale'''
        return cls.available_products > 0

    def __init__(self):
        self.myLock = Lock()
        print("[CHECKOUT] Checkout open. Available products: " + str(Till.available_products))

    def pay(self, customer):
        '''Allows the client to pay the product on sale, which makes it one less item'''
        print("[CHECKOUT] Client (" + customer.getName() + ") is going to pay...")
        # Fill...
        if self.hasProducts():
            Till.available_products -= 1
            print("[CHECKOUT] Client (" + customer.getName() + ") has paid")
            print("[CHECKOUT] Available products: " + str(Till.available_products))
        else:
            print("[CHECKOUT] Closed checkout. There are no products left")
        

class Customer(Thread):
    '''Simulates the behaviour of the mall clients'''

    MAX_TIME = 10

    @classmethod
    def setTill(cls, till):
        cls.sharedTill = till

    def __init__(self, number):
        Thread.__init__(self)
        self.name = str(number)

    def getName(self):
        return self.name

    def walk(self):
        '''Goes for a walk for a while'''
        print("[CLIENT " + self.name + "] I'll go for a walk...")
        sleep(uniform(1, Customer.MAX_TIME))

    def hasToBuy(self):
        '''Decides if its going to buy the product'''
        return choice([True, False])

    def run(self):
        self.walk()
        # Fill...
        if self.sharedTill.hasProducts():
            if self.hasToBuy():
                if self.sharedTill.myLock.locked():
                    print("[CLIENT " + self.name + "] Checkout is occupied, ill wait")
                self.sharedTill.myLock.acquire()
                print("[CLIENT " + self.name + "] Now i'm at the checkout, lets pay...")
                self.sharedTill.pay(self)
                self.sharedTill.myLock.release()
                print("[CLIENT " + self.name + "] I'm going home with my new product (leaving checkout)")
        

if __name__ == "__main__":
    myTill = Till()
    Customer.setTill(myTill)

    # Fill (create threads and launch them)
    for i in range(NUMBER_OF_CUSTOMERS):
        customer = Customer(i)
        customer.start()
