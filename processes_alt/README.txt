Puedo observar dos comportamientos:

1) 200 subprocesos sin espera hasta 10 segundos de sleep maximo

  En este caso se crean los subprocesos y saturan al procesador durante un momento por la creación de esa gran
  cantidad de hilos. Después el procesador deja de estar tan saturado y comienza a ir cerrando progresivamente los
  subprocesos dependiendo del valor random que tengan de sleep.

2)  200 subprocesos con espera hasta 10 segundos de sleep maximo

  En este caso se va creando un subproceso, esperando su valor random de sleep y así hasta 200. En este caso nunca
  se satura el procesador ya que entra un subproceso, hace su labor y viene el siguiente.