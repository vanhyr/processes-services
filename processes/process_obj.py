from time import sleep
from random import randint
from multiprocessing import Process

from sys import argv


class SubProcess(Process):

    def __init__(self, max_naptime):
        super().__init__()
        self.naptime = randint(1, max_naptime)
        #print(self.napTime)

    def run(self):
        print("(S) Hello, I'm a new subprocess and I'm going to sleep")
        sleep(self.naptime)
        print("(S) I'm awake, such a good nap. I slept: " + str(self.naptime) + " seconds")


# Launchs the subprocesses with specified values
def _run(sp_number, wait, max_naptime):
    print("(M) Hello I'm the master process and I'm goind to create a subprocess")
    subprocesses = []
    for i in range(sp_number):
        sp = SubProcess(max_naptime)
        subprocesses.append(sp)
        sp.start()    

    if wait == "-w":
        for sp in subprocesses:
            sp.join()
                  
    print("(M) I'm done, bye!")


# Shows an error and suggest valid format
def _error(code = 1):
    if code == 2:
        print("Please enter 3 valid args (int):[sp_number] (string):[-w/-c] (int):[max_naptime]")
        print("No args for defaults: 2 -w 10")
    elif code == 1:
        print("Please enter 3 valid args (int):[sp_number] (string):[-w/-c] (int):[max_naptime]")

if __name__ == '__main__':

    args = argv[1:]

    # If all args are passed check if type is correct and values are valid
    if len(args) == 3:
        try:
            sp_number = int(args[0])
            wait = args[1]
            max_naptime = int(args[2])
            if sp_number > 0 and max_naptime > 0 and max_naptime < 61 and wait == "-w" or wait == "-c":
                _run(sp_number, wait, max_naptime)
            else:
                _error()
        except TypeError:
            _error()
    # If no args passed do defaults
    elif len(args) == 0:
        sp_number = 2
        wait = "-w"
        max_naptime = 10
        _run(sp_number, wait, max_naptime)
    # If the number of args passed is wrong
    else:
        _error(2)
