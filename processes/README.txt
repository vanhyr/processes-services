Puedo observar que se crean los subprocesos y saturan al procesador durante un momento por la creación de esa gran
cantidad de hilos. Después el procesador deja de estar tan saturado y comienza a ir cerrando progresivamente los
subprocesos dependiendo del valor random que tengan de sleep.